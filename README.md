# Task specyfics
- 2D gravity simulation on spheres
- Total of 250 spheres, spawned in 0.25 second intervals
    - After the total of 250 spheres have been spawned gravitional pull is reversed, that is objects start pushing at each other as if charged by same-signed electric potential
- Upon collision the spheres merge, adding up their area of surface and mass
    - When a sphere reaches a critical mass of 50 base spheres it explodes into 50 spheres that are spread in random directions, and ignore collisions for 0.5 seconds

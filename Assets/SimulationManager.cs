﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

namespace Gravity
{
    public class SimulationManager : MonoBehaviour
    {
        public static SimulationManager Instance { get; private set; }

        [SerializeField]
        private TextMeshProUGUI sphereCounter;

        [SerializeField]
        private float spawnIntervals;


        private List<SphereController> spheres = new List<SphereController>();
        private int numberOfSpawnedSpheres = 0;

        private bool reverseGravity;

        private SpherePoolManager spherePoolManager;

        private void Awake() 
        {
            if (Instance != null) 
            {
                throw new System.Exception("Double singleton!");
            }

            Instance = this;

            spherePoolManager = GetComponent<SpherePoolManager>();

            StartCoroutine(BallSpawnCoroutine());
        }

        private IEnumerator BallSpawnCoroutine() 
        {
            // Spawn set amont of balls before reversing gravity
            while (numberOfSpawnedSpheres < spherePoolManager.PoolSize)
            {
                SphereController newSphere = spherePoolManager.GetSphereFromPool();
                newSphere.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height)));

                spheres.Add(newSphere);
                ++numberOfSpawnedSpheres;

                sphereCounter.text = numberOfSpawnedSpheres.ToString();

                yield return new WaitForSeconds(spawnIntervals);
            }

            reverseGravity = true;
        }

        private void FixedUpdate()
        {
            foreach (SphereController ball in spheres)
            {
                ball.ResultantForce = Vector3.zero;
            }

            List<SphereController> unprocessedBalls = new List<SphereController>(spheres);

            foreach (SphereController ball in spheres)
            {
                unprocessedBalls.Remove(ball);

                foreach (SphereController otherBall in unprocessedBalls)
                {
                    float ballGravity = CalculateGravity(ball, otherBall);

                    if (reverseGravity)
                    {
                        ballGravity = -ballGravity;
                    }

                    ball.ResultantForce += (otherBall.transform.position - ball.transform.position).normalized * ballGravity;
                    otherBall.ResultantForce += (ball.transform.position - otherBall.transform.position).normalized * ballGravity;
                }

                ball.ApplyForce();
            }
        }

        private float CalculateGravity(SphereController ball, SphereController otherBall) 
        {
            return (ball.SphereRigidBody.mass * otherBall.SphereRigidBody.mass) / Mathf.Max(Mathf.Pow(Vector3.Distance(ball.transform.position, otherBall.transform.position), 2), 0.001f);
        }

        public void ManageCollision(Collision2D collision) 
        {
            // Bigger/heavier ball stays
            SphereController survivingBall, deletedBall;

            if (collision.collider.GetComponent<Rigidbody2D>().mass >= collision.otherCollider.GetComponent<Rigidbody2D>().mass)
            {
                survivingBall = collision.collider.GetComponent<SphereController>();
                deletedBall = collision.otherCollider.GetComponent<SphereController>();
            }
            else
            {
                deletedBall = collision.collider.GetComponent<SphereController>();
                survivingBall = collision.otherCollider.GetComponent<SphereController>();
            }

            survivingBall.SphereRigidBody.mass += deletedBall.SphereRigidBody.mass;
            if (survivingBall.SphereRigidBody.mass >= 50 * spherePoolManager.SpherePrefab.GetComponent<Rigidbody2D>().mass)
            {
                //Explode
                spheres.Remove(survivingBall);
                Vector3 survivingBallPosition = survivingBall.transform.position;

                spherePoolManager.ReturnSphereToPool(survivingBall);
                
                for (int i = 0; i < 50; ++i)
                {
                    SphereController newBall = spherePoolManager.GetSphereFromPool();
                    newBall.transform.position = survivingBallPosition;
                    spheres.Add(newBall);
                    newBall.SphereRigidBody.AddForce(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0).normalized * 50f, ForceMode2D.Impulse);
                    newBall.DisableCollisionFor(0.5f);
                }
            }
            else
            {
                //Enlarge
                survivingBall.transform.localScale = CalculateNewBallScale(survivingBall.transform, deletedBall.transform);
            }

            spheres.Remove(deletedBall);
            spherePoolManager.ReturnSphereToPool(deletedBall);
        }

        private Vector3 CalculateNewBallScale(Transform ball, Transform otherBall) 
        {
            float newArea = Mathf.PI * Mathf.Pow(ball.localScale.x / 2, 2) + Mathf.PI * Mathf.Pow(otherBall.localScale.x / 2, 2);
            float newR = Mathf.Sqrt(newArea / Mathf.PI);

            return new Vector3(newR * 2, newR * 2, newR * 2);
        }
    }
}
﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gravity
{
    public class SpherePoolManager : MonoBehaviour
    {
        [SerializeField]
        private SphereController spherePrefab;
        public SphereController SpherePrefab { get => spherePrefab; private set => spherePrefab = value; }

        [SerializeField]
        private int poolSize;
        public int PoolSize { get => poolSize; private set => poolSize = value; }


        // Inavtive objects ready for pulling
        private Queue<SphereController> projectilePool = new Queue<SphereController>();
        // Chronological references to pulled objects, in case poll gets emptied
        private List<SphereController> spawnedObjects = new List<SphereController>();


        private void Awake()
        {
            for (int i = 0; i < PoolSize; ++i)
            {
                SphereController newProjectileInstance = Instantiate(SpherePrefab, transform);
                newProjectileInstance.gameObject.SetActive(false);

                projectilePool.Enqueue(newProjectileInstance);
            }
        }

        public SphereController GetSphereFromPool()
        {
            SphereController pulledProjectile;

            if (projectilePool.Count > 0)
            {
                // get first object from pool
                pulledProjectile = projectilePool.Dequeue();
                pulledProjectile.gameObject.SetActive(true);
                spawnedObjects.Add(pulledProjectile);
            }
            else
            {
                // get last spawned object and move it to back of queue
                pulledProjectile = spawnedObjects.First();
                spawnedObjects.RemoveAt(0);
                spawnedObjects.Add(pulledProjectile);
            }

            // resetromg to default values on pull
            pulledProjectile.transform.localScale = spherePrefab.transform.localScale;
            pulledProjectile.SphereRigidBody.mass = spherePrefab.SphereRigidBody.mass;

            return pulledProjectile;
        }

        // Manual call to put object back to pool
        public void ReturnSphereToPool(SphereController returnedSphere)
        {
            spawnedObjects.Remove(returnedSphere);
            returnedSphere.gameObject.SetActive(false);
            projectilePool.Enqueue(returnedSphere);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gravity
{
    public class SphereController : MonoBehaviour
    {
        [SerializeField]
        Rigidbody2D sphereRigidbody;
        public Rigidbody2D SphereRigidBody { get => sphereRigidbody; set => sphereRigidbody = value; }

        public Vector3 ResultantForce { get; set; } = Vector3.zero;

        private Coroutine collisionDisabledCoroutine;

        public void ApplyForce() 
        {
            SphereRigidBody.AddForce(ResultantForce);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            // avoiding double collision reports
            if (collision.otherCollider.gameObject.GetInstanceID() > collision.collider.gameObject.GetInstanceID())
            {
                SimulationManager.Instance.ManageCollision(collision);
            }
        }

        public void DisableCollisionFor(float time)
        {
            gameObject.layer = 8;

            if (collisionDisabledCoroutine != null) 
            {
                StopCoroutine(collisionDisabledCoroutine);
            }

            collisionDisabledCoroutine = StartCoroutine(RenewCollisionfter(time));
        }

        private IEnumerator RenewCollisionfter(float time) 
        {
            yield return new WaitForSeconds(time);

            gameObject.layer = 0;
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }
    }
}